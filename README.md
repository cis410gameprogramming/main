# Slug Wranglers #
### HOW TO PLAY: ###

-You are the cowboy in this game to navigate through different parts of the world. Your goal in each level is to survive to get to the next. The purple warp will take you to the next level.

-There are red and green bars on the top left of the screen. The red bar represents player health and green bar represents player stamina.

-Hold down "Shift" to sprint. Sprinting will cause stamina to go down, but stamina will regain if you are idle (not moving).

-Press "Space" to dash at the cost of stamina

-Press "P" to attack enemies